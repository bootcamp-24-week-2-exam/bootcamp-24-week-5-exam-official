import { createContext } from "react";

const StudentContext = createContext(null);

// export context
export default StudentContext;