import "./App.css";
import AddStudent from "./Components/AddStudent";
import axios from "axios";
import React, { useState, useEffect } from "react";
import FilterStudent from "./Components/FilterStudent";

function App() {
  const h1Design = {
    textAlign: "center",
    fontSize: "72px",
    fontFamily: "Georgia",
    color: "#0d84a8",
  };

  //pagination start
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage, setPostsPerPage] = useState(5);

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      const res = await axios.get("http://localhost:8080/search/byAll");
      setPosts(res.data);
      // console.log(res.data);
      setLoading(false);
    };

    fetchPosts();
  }, []);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  // const currentPosts = posts.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = (pageNumber) => setCurrentPage(pageNumber);
  //pagination end

  // use state of student inside StudentContext.
  // const [newStudent, setNewStudent] = useState([],[]);

  return (
    // <StudentContext.Provider value={{newStudent, setNewStudent}}>
    //   <StudentByAll />
    // </StudentContext.Provider>
    <div className="container">
      <h1 style={h1Design}>Student Information System</h1>
      <div className="controls-and-filters">
        {/* put add component, and search filters here. */}
      </div>
      <div>
        {/* put StudentAll component here */}

        <FilterStudent />
      </div>
    </div>
  );
}

export default App;
