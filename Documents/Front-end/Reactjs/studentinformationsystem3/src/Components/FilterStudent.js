import React, { useState } from "react";
import { Button, Form, Alert } from "react-bootstrap";
import Filters from "./Service/Filters";
import StudentAll from "./Service/StudentAll";
import SectionList from "./Service/SectionList";
import AddStudent from "./AddStudent";
import "./FilterStudent.css";

const FilterStudent = () => {
  const [selectedFilter, setSelectedFilter] = useState("");
  const [section, setSection] = useState(1);
  const [showing, setShowing] = useState(true);
  const [showingSearch2, setShowingSearch2] = useState(false);
  const [search, setSearch] = useState("");
  const [search2, setSearch2] = useState("");
  const [placeHolder, setPlaceHolder] = useState("");
  const [textFieldEnable, setTextFieldEnable] = useState(true);
  const [searchBtnEnable, setSearchBtnEnable] = useState(true);
  const [minMax, setMinMax] = useState(false);
  const [showingSection, setShowingSection] = useState(false);

  const [api, setApi] = useState("byAll");

  const [showSearchTable, setShowSearchTable] = useState(false);

  const handleFilterSelect = (e) => {
    setTextFieldEnable(false);
    setSearchBtnEnable(true);
    setShowingSection(false);
    setSearch("");
    setSearch2("");
    setSelectedFilter(e.target.value);
    switch (e.target.value) {
      case "1":
        setShowingSearch2(true);
        setPlaceHolder("Year level");
        break;
      case "2":
        setShowingSection(true);
        setShowingSearch2(false);
        setSearchBtnEnable(false);
        setPlaceHolder("Section");
        break;
      case "3":
        setShowingSearch2(false);
        setPlaceHolder("City");
        break;
      case "4":
        setShowingSearch2(true);
        setPlaceHolder("Zip");
        break;
      case "5":
        //setShowing(false);
        setShowingSearch2(true);
        setPlaceHolder("Minimum age");
        break;
      case "6":
        //setShowing(true);
        setShowingSearch2(false);
        setPlaceHolder("Student ID no or name");
        break;
      default:
        break;
    }
  };

  const handleClick = (e) => {
    //e.preventDefault();
    setMinMax(false);
    setShowing(false);
    switch (selectedFilter) {
      case "1":
        setApi("byYrLvl?yrlvl=" + search);
        break;
      case "2":
        setApi("bySec?section=" + section);
        break;
      case "3":
        setApi("byCity?city=" + search);
        break;
      case "4":
        setApi("byZip?zip=" + search);
        break;
      case "5":
        if (parseInt(search) < parseInt(search2)) {
          setApi("byAge?minimum=" + search + "&maximum=" + search2);
        } else {
          setMinMax(true);
        }
        break;
      case "6":
        setApi("" + search);
        break;
      default:
        console.log("none selected");
        break;
    }
    setShowing(true);
  };

  const handleChange = (e) => {
    setMinMax(false);
    setSearch(
      showingSearch2 ? e.target.value.replace(/\D/g, "") : e.target.value
    );
    setSearchBtnEnable(e.target.value !== "" ? false : true);
    if (selectedFilter === "5") {
      //setMinMax(minmaxEnable(e.target.value, search2));
      //setSearchBtnEnable(minmaxEnable(e.target.value, search2));
    }
  };

  const handleChange2 = (e) => {
    setMinMax(false);
    setSearch2(
      showingSearch2 ? e.target.value.replace(/\D/g, "") : e.target.value
    );
    setSearchBtnEnable(e.target.value !== "" ? false : true);
    if (selectedFilter === "5") {
      //setMinMax(minmaxEnable(search, e.target.value));
      //setSearchBtnEnable(minmaxEnable(search, e.target.value));
    }
  };

  const filterStyle = {
    fontFamily: "Courier New",
  };

  return (
    <main>
      <div className="div">
        <label className="filterby-style" style={filterStyle}>
          Filter by :
        </label>
        <Form.Group className="form-group">
          <Form.Select
            className="rounded-0 shadow"
            value={selectedFilter}
            onChange={handleFilterSelect}
          >
            <option value="" disabled>
              Select
            </option>
            {Filters.map(({ id, tag }) => (
              <option key={id} value={id}>
                {tag}
              </option>
            ))}
          </Form.Select>
        </Form.Group>
        {!showingSection && (
          <Form.Control
            id="search"
            type="text"
            className="input"
            disabled={textFieldEnable}
            value={search}
            placeholder={placeHolder}
            onChange={handleChange}
          />
        )}
        {showingSection && (
          <SectionList section={section} setSection={setSection} />
        )}
        {showingSearch2 && selectedFilter === "5" && (
          <Form.Control
            id="search2"
            type="text"
            className="input"
            value={search2}
            placeholder="Maximum Age"
            onChange={handleChange2}
          />
        )}
        <Button
          className="search-btn"
          variant="primary"
          type="submit"
          disabled={searchBtnEnable}
          onClick={handleClick}
        >
          Search
        </Button>
      </div>

      <AddStudent />

      {minMax && (
        <Alert key="danger" variant="danger">
          Error:<br></br> {search} is greater than maximum age
        </Alert>
      )}

      {showing && <StudentAll api={api} />}
    </main>
  );
};

export default FilterStudent;
