import React, { useEffect, useState } from "react";
import { Table, Button } from "react-bootstrap";
import { Modal } from "react-bootstrap";
import { Form } from "react-bootstrap";
import axios from "axios";
import SectionList from "./Service/SectionList";

const Posts = ({ posts, loading, setgetAllStudents, getAllStudents }) => {
  // for modals.
  // for view
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  // for delete
  // const [showdelete, setShowdelete] = useState(false);
  // const handleCloseDelete = () => setShow(false);
  // const handleShowDelete = () => setShow(true);

  const [showupdate, setShowupdate] = useState(false);
  const handleCloseUpdate = () => setShowupdate(false);
  const handleShowUpdate = () => setShowupdate(true);

  const [viewstudent, setViewStudent] = useState([]);
  const [updatestudent, setUpdateStudent] = useState([]);

  // for inputs
  let [firstname, setfirstName] = useState("");
  const [lastname, setlastName] = useState("");
  const [middlename, setmiddleName] = useState("");
  const [address, setAddress] = useState("");
  const [age, setAge] = useState("");
  const [zip, setZIP] = useState("");
  const [city, setCity] = useState("");
  const [regionprovince, setRegionProvince] = useState("");
  const [phone, setPhone] = useState("");
  const [mobile, setMobile] = useState("");
  const [email, setEmail] = useState("");
  const [yrlevel, setYrLevel] = useState("");
  const [section, setSection] = useState("");
  const [sections, setSections] = useState("");

  // functions for onchange.
  const handleFirstname = (e) => {
    setfirstName(e.target.value);
    // console.log(e.target.value);
  };
  const handleLastname = (e) => {
    setlastName(e.target.value);
    // console.log(e.target.value);
  };
  const handleMiddlename = (e) => {
    setmiddleName(e.target.value);
    // console.log(e.target.value);
  };
  const handleAddress = (e) => {
    setAddress(e.target.value);
    // console.log(e.target.value);
  };
  const handleAge = (e) => {
    setAge(e.target.value);
    // console.log(e.target.value);
  };
  const handleZIP = (e) => {
    setZIP(e.target.value);
    // console.log(e.target.value);
  };
  const handleCity = (e) => {
    setCity(e.target.value);
    // console.log(e.target.value);
  };
  const handleRegionProvince = (e) => {
    setRegionProvince(e.target.value);
    // console.log(e.target.value);
  };
  const handlePhone = (e) => {
    setPhone(e.target.value);
    // console.log(e.target.value);
  };
  const handleMobile = (e) => {
    setMobile(e.target.value);
    // console.log(e.target.value);
  };
  const handleEmail = (e) => {
    setEmail(e.target.value);
    // console.log(e.target.value);
  };
  const handleYrlevel = (e) => {
    setYrLevel(e.target.value);
    // console.log(e.target.value);
  };
  const handleSection = (e) => {
    setSection(e.target.value);
    // console.log(e.target.value);
  };

  if (loading) {
    return <h2>Loading...</h2>;
  }

  // view student information.
  const handleView = (student) => {
    console.log("handleView:", student);
    setViewStudent(student);
  };

  //getid delete student.
  const handleDelete = (student) => {
    console.log("handleDelete:", student);
    const urlRequest = "http://localhost:8080/delete/" + student.id;
    console.log(urlRequest);
    axios
      .delete(urlRequest)
      .then((response) => {
        console.log("Delete success!", response);
        setgetAllStudents(
          getAllStudents.filter((item) => item.id !== student.id)
        );
      })
      .catch((error) => {
        console.error("There was an error!", error);
      });
  };

  const handleUpdate = (student) => {
    console.log("handleUpdate:", student);
    setUpdateStudent(student);
    setfirstName(student.firstname);
    setlastName(student.lastname);
    setmiddleName(student.middlename);
    setAddress(student.address);
    setAge(student.age);
    setZIP(student.zip);
    setCity(student.city);
    setRegionProvince(student.regionProvince);
    setPhone(student.phoneNo);
    setMobile(student.mobileNo);
    setEmail(student.email);
    setYrLevel(student.yrlvl);
    setSection(student.sectionId);
  };

  const handleUpdateYes = () => {
    let updatenewStudent = {
      id: updatestudent.id,
      firstname: firstname,
      lastname: lastname,
      middlename: middlename,
      age: age,
      address: address,
      zip: zip,
      city: city,
      regionProvince: regionprovince,
      phoneNo: phone,
      mobileNo: mobile,
      email: email,
      yrlvl: yrlevel,
      sectionId: section,
    };
    const updatedData = getAllStudents.map((obj) => {
      if (parseInt(obj.id) === parseInt(updatestudent.id)) {
        return {
          ...obj,
          firstname: firstname,
          lastname: lastname,
          middlename: middlename,
          age: age,
          address: address,
          zip: zip,
          city: city,
          regionProvince: regionprovince,
          phoneNo: phone,
          mobileNo: mobile,
          email: email,
          yrlvl: yrlevel,
          sectionId: section,
          sections: sections,
        };
      }

      return obj;
    });

    axios
      .put("http://localhost:8080/edit/" + updatestudent.id, updatenewStudent, {
        auth: {
          username: "root",
          password: "root",
        },
      })
      .then((response) => {
        console.log("Update Success!", response);
        console.log("Update Success!", updatedData);
        setgetAllStudents(updatedData);
      })
      .catch((error) => {
        console.log("There was an error!", error);
      });
  };

  return (
    <>
      {/* Modal for View */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Student Information</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>First name</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                value={viewstudent.firstname}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formLastname">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                name="lname"
                type="text"
                value={viewstudent.lastname}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formMiddlename">
              <Form.Label>Middle name</Form.Label>
              <Form.Control
                name="mname"
                type="text"
                value={viewstudent.middlename}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formAddress">
              <Form.Label>Address</Form.Label>
              <Form.Control
                name="address"
                type="text"
                value={viewstudent.address}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formAge">
              <Form.Label>Age</Form.Label>
              <Form.Control
                name="age"
                type="text"
                value={viewstudent.age}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formZip">
              <Form.Label>Zip</Form.Label>
              <Form.Control
                name="zip"
                type="text"
                value={viewstudent.zip}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formCity">
              <Form.Label>City</Form.Label>
              <Form.Control
                name="city"
                type="text"
                value={viewstudent.city}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formRegionProvince">
              <Form.Label>Region Province</Form.Label>
              <Form.Control
                name="regionprovince"
                type="text"
                value={viewstudent.regionProvince}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formPhoneNo">
              <Form.Label>Phone No.</Form.Label>
              <Form.Control
                name="phoneNo"
                type="text"
                value={viewstudent.phoneNo}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formMobileNo">
              <Form.Label>Mobile No.</Form.Label>
              <Form.Control
                name="mobileNo"
                type="text"
                value={viewstudent.mobileNo}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                name="email"
                type="text"
                value={viewstudent.email}
                readOnly
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formYearLevel">
              <Form.Label>Year Level</Form.Label>
              <Form.Select name="yrlvl" value={viewstudent.yrlvl} disabled>
                <option>Year Level...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formSection">
              <Form.Label>Section</Form.Label>
              <Form.Select
                name="section"
                value={viewstudent.sectionId}
                disabled
              >
                <option value="1">Saint Francis</option>
                <option value="2">Saint Paul</option>
                <option value="3">Saint Therese</option>
                <option value="4">Saint John</option>
                <option value="5">Saint Peter</option>
                <option value="6">Saint Luke</option>
              </Form.Select>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" type="submit" onClick={handleClose}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>

      {/* modal for delete */}
      {/* <Modal show={showdelete} onHide={handleCloseDelete}>
            <Modal.Header closeButton>
            <Modal.Title>Deleting Student</Modal.Title>
            </Modal.Header>
            <Modal.Body>

            </Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" type="submit" onClick={handleCloseDelete}>
                Cancel
            </Button>
            </Modal.Footer>
        </Modal> */}

      {/* Modal for Update */}
      <Modal
        show={showupdate}
        onHide={() => {
          handleCloseUpdate();
        }}
      >
        <Modal.Header closeButton>
          <Modal.Title>Update Student</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>First name</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                placeholder="Enter First name"
                defaultValue={firstname}
                onChange={handleFirstname}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={lastname}
                onChange={handleLastname}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Middle name</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={middlename}
                onChange={handleMiddlename}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Address</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={address}
                onChange={handleAddress}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Age</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={age}
                onChange={handleAge}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>ZIP</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={zip}
                onChange={handleZIP}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>City</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={city}
                onChange={handleCity}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Region/Province</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={regionprovince}
                onChange={handleRegionProvince}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Phone no.</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={phone}
                onChange={handlePhone}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Mobile no.</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={mobile}
                onChange={handleMobile}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Email</Form.Label>
              <Form.Control
                name="fname"
                type="text"
                defaultValue={email}
                onChange={handleEmail}
                required
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Year Level</Form.Label>
              {/* <Form.Control
                name="fname"
                type="text"
                defaultValue={yrlevel}
                onChange={handleYrlevel}
                required
              /> */}
              <Form.Select
                name="yrlvl"
                defaultValue={yrlevel}
                onChange={handleYrlevel}
                required
              >
                <option disabled>Year Level...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
              </Form.Select>
            </Form.Group>
            <Form.Group className="mb-3" controlId="formFirstname">
              <Form.Label>Section ID</Form.Label>
              {/* <Form.Control
                name="fname"
                type="text"
                placeholder="Enter first name"
                required
                defaultValue={section}
                onChange={handleSection}
              /> */}
              <SectionList
                section={section}
                setSection={setSection}
                setSections={setSections}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            type="submit"
            onClick={() => {
              handleCloseUpdate();
            }}
          >
            Cancel
          </Button>
          <Button
            variant="primary"
            type="submit"
            onClick={() => {
              handleCloseUpdate();
              handleUpdateYes();
            }}
          >
            Save
          </Button>
        </Modal.Footer>
      </Modal>

      <Table striped bordered hover size="sm" responsive="sm">
        <thead>
          <tr>
            <th>Student ID</th>
            <th>Last name</th>
            <th>First name</th>
            <th>Middle name</th>
            <th>Age</th>
            <th>ZIP code</th>
            <th>City</th>
            <th>Year Level</th>
            <th>Section</th>
            <th>Action</th>
          </tr>
        </thead>

        {/* student data */}
        <tbody>
          {posts.map((students) => {
            return (
              <tr key={students.id}>
                <td>{students.id}</td>
                <td>{students.lastname}</td>
                <td>{students.firstname}</td>
                <td>{students.middlename}</td>
                <td>{students.age}</td>
                <td>{students.zip}</td>
                <td>{students.city}</td>
                <td>{students.yrlvl}</td>
                <td>{students.sections}</td>
                <td>
                  <Button
                    className="view-btn"
                    variant="primary"
                    type="submit"
                    value={students.id}
                    onClick={() => {
                      handleView(students);
                      handleShow();
                    }}
                  >
                    View
                  </Button>
                  <Button
                    className="delete-btn"
                    variant="danger"
                    type="submit"
                    value={students.id}
                    onClick={() => {
                      handleDelete(students);
                    }}
                  >
                    Delete
                  </Button>
                  <Button
                    className="update-btn"
                    variant="success"
                    type="submit"
                    value={students.id}
                    onClick={() => {
                      handleUpdate(students);
                      handleShowUpdate();
                    }}
                  >
                    Update
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
    </>
  );
};

export default Posts;
