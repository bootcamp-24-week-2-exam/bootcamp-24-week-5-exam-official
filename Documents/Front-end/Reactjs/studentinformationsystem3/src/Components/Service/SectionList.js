import axios from "axios";
import { useState, useEffect } from "react";
import { Form } from "react-bootstrap";

const SectionList = ({ section, setSection, setSections }) => {
  const [getSectionList, setgetSectionList] = useState([]);

  //pagination start
  const [loading, setLoading] = useState(false);

  // get all sections
  useEffect(() => {
    setLoading(true);
    axios
      .get("http://localhost:8080/sections")
      .then((response) => {
        setgetSectionList(response.data);
        // console.log(response.data)
        // setting new student data with the new response.
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  const handleSection = (e) => {
    setSection(e.target.value);
    const [sections] = getSectionList
      .filter(({ id }) => id === parseInt(e.target.value))
      .map(({ section }) => ({ sections: section }));

    console.log("handleSection:", sections.sections);

    // console.log(
    //   "handleSection:",
    //   getSectionList
    //     .filter(({ id }) => id === parseInt(e.target.value))
    //     .map(({ section }) => ({ sections: section }))
    // );
    setSections(sections.sections);
  };

  return (
    <div>
      <Form.Group className="mb-3" controlId="formSection">
        <Form.Select
          name="section"
          required
          value={section}
          aria-label="Default select example"
          onChange={handleSection}
        >
          <option disabled>Seclect Section</option>
          {getSectionList.map(({ id, section }) => {
            return (
              <option key={id} value={id}>
                {section}
              </option>
            );
          })}
        </Form.Select>
      </Form.Group>
    </div>
  );
};

export default SectionList;
